<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Document</title>
</head>
<body>

  <h1>Form Pendaftaran</h1>
  <h3>Sign Up Form</h3>

  <form action="/welcome" method="POST" class="form-horizontal">
                            @csrf

                            <div class="form-group">
                                <label for="namadepan">First Name</label>
                                <input type="smallinteger" class="form-control @error('namadepan') is-invalid @enderror"
                                    name="namadepan" value="{{ old('namadepan') }}" required>

                                <!-- error message untuk title -->
                                @error('namadepan')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
 
    <label>First Name:</label><br><br>
    <input type="text" class="form-control @error('namadepan') is-invalid @enderror"
    name="namadepan" value="{{ old('namadepan') }}" required><br><br>
    <label>Last Name:</label><br><br>
    <input type="text" name="namabelakang"><br><br>
    <label>Gender:</label><br><br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br>
    <input type="radio" name="gender">Other<br><br>
    <label for="nationality">Nationality:</label><br><br>
    <select name="nationality" class="form-control" required>
    <option value="0" selected>Australian</option>
    <option value="1" selected>Malaysian</option>
    <option value="2" selected>Singaporean</option>
    <option value="3" selected>Indonesian</option>
    </select><br><br>
    <label>Language Spoken:</label><br><br>
    <input type="checkbox"> Bahasa Indonesia<br>
    <input type="checkbox"> English<br>
    <input type="checkbox"> Other<br><br>
    <label>Bio</label><br><br>
      <textarea name="bio" cols="30" rows="10"></textarea><br><br>
      <button type="submit" class="btn btn-md btn-primary">SIGN UP</button>
  </form>

</body>
</html>
