<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('posts/home');
});

Route::get('/welcome', function () {
    return view('posts/welcome');
});

Route::get('/register', function () {
    return view('posts/register');
});

Route::resource('post', AuthController::class);

Route::resource('get', HomeController::class);


Route::get('/master', function () {
    return view('adminlte/master');
});

Route::get('/data-tables', function () {
    return view('items/data-tables');
});

Route::get('/table', function () {
    return view('items/table');
});

Route::get('/items/create', function () {
    return view('items/create');
});

Route::get('/casts/create', 'CastController@create');

Route::post('/casts', 'CastController@store');

Route::get('/casts', 'CastController@index');

Route::get('/casts/{id}', 'CastController@show');

Route::get('/casts/{id}/edit', 'CastController@edit');

Route::put('/casts/{id}', 'CastController@update');

Route::delete('/casts/{id}', 'CastController@destroy');