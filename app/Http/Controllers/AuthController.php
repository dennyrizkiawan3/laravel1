<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome()
    {
        $posts = Post::latest()->get();
        return view('posts.welcome', compact('posts')); //MENGAMBIL DATA POST KEMUDIAN PASSING KE VIEW welcome.BLADE.PHP
    } //

    public function register() //TAMBAHAN 2 UNTUK MENAMPILKAN HALAMAN register.BLADE.PHP
    {
        return view('posts.register');
    } //

        // TAMBAHAN 3 
        public function store(Request $request)
        {
            $this->validate($request, [   //VALIDASI
                'namadepan' => 'required',
               
            ]);
    
            $post = Post::register([
                'namadepan' => $request->namadepan,
                
            ]);
    
            if ($post) {
                return redirect()
                    ->route('post.welcome')
                    ->with([
                        'success' => 'Halo'
                    ]);
            } else {
                return redirect()
                    ->back()
                    ->withInput()
                    ->with([
                        'error' => 'Ada kesalahan, coba lagi'
                    ]);
            }
        } //
}
